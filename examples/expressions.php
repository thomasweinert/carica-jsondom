<?php

include(__DIR__ . '/../src/Carica/JsonDOM/Reader.php');

$json = file_get_contents(__DIR__.'/files/address.json');

$reader = new Carica\JsonDOM\Reader($json);
$dom = $reader->asDom();
$xpath = new DOMXpath($dom);

$expressions = array(
  'First name: ' => 'string(/*/firstName)',
  'Last name: ' => 'string(/*/lastName)',
  'Name (Age): ' => 'concat(/*/firstName, " ", /*/lastName, " (", /*/age ,")")',
  'Phone number: ' => 'string(/*/phoneNumbers/*[type="home"]/number)',
  'Second number: ' => 'concat(/*/phoneNumbers/*[2]/type, " ", /*/phoneNumbers/*[2]/number)',
  'Number count: ' => 'count(/*/phoneNumbers/*)'
);

foreach ($expressions as $caption => $expression) {
  echo $caption, $xpath->evaluate($expression, NULL, FALSE), "\n";
  echo $expression, "\n\n";
}