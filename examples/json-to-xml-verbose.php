<?php

include(__DIR__ . '/../src/Carica/JsonDOM/Reader.php');

$json = file_get_contents(__DIR__.'/files/address.json');

$reader = new Carica\JsonDOM\Reader($json, TRUE);
$dom = $reader->asDom();

$dom->formatOutput = TRUE;
echo $dom->saveXml(), "\n\n";
