<?php
include(__DIR__ . '/../src/Carica/JsonDOM/Reader.php');
// load json example from http://en.wikipedia.org/wiki/JSON
$json = file_get_contents(__DIR__.'/files/address.json');
// define import
$reader = new Carica\JsonDOM\Reader($json);
// import and return xpath object
$xpath = new DOMXpath($reader->asDom());
// output home phone number
var_dump(
  $xpath->evaluate('string(/*/phoneNumbers/*[type="home"]/number)')
);
// string(12) "212 555-1234"