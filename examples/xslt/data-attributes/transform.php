<?php
$xslDom = new DOMDocument();
$xslDom->load(__DIR__.'/template.xsl');
$xslt = new XSLTProcessor();
$xslt->importStylesheet($xslDom);
unset($xslDom);

$xml = <<<'XML'
<texts>
  <text id="message">Hello World!</text>
</texts>
XML;

$dom = new DOMDocument();
$dom->loadXml($xml);

echo $xslt->transformToXml($dom);