<?php

include(__DIR__ . '/../src/Carica/JsonDOM/Reader.php');
include(__DIR__ . '/../src/Carica/JsonDOM/Writer.php');

$json = file_get_contents(__DIR__.'/files/address.json');

echo 'Original Json: ', "\n";
echo $json, "\n\n";

$reader = new Carica\JsonDOM\Reader($json);
$dom = $reader->asDom();

echo 'Json -> XML: ', "\n";
$dom->formatOutput = TRUE;
echo $dom->saveXml(), "\n\n";

echo 'XML -> JSON: ', "\n";
$writer = new Carica\JsonDOM\Writer($dom);
echo json_encode($writer, JSON_PRETTY_PRINT);