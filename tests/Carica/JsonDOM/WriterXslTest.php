<?php

namespace Carica\JsonDOM {

  class WriterXslTest extends \PHPUnit_Framework_TestCase {

    /**
     * @dataProvider provideStringExamples
     * @param string $expected
     * @param string $string
     */
    public function testStringEncode($expected, $string) {
      $xslt = $this->getProcessor('
        <xsl:template match="/">
          <xsl:variable name="string">'.htmlspecialchars($string).'</xsl:variable>
          <xsl:value-of select="json:encode-string($string)"/>
        </xsl:template>
      ');
      $this->assertEquals(
        $expected,
        $xslt->transformToXml($this->getDocument())
      );
    }

    /**
     * @dataProvider provideXmlToJsonExamples
     * @param string $json
     * @param string $xml
     * @param bool $isDirty
     */
    public function testXmlToJson($json, $xml, $isDirty = FALSE) {
      $xslt = $this->getProcessor('
        <xsl:param name="isDirty" select="false()"/>
        <xsl:template match="/*">
          <xsl:variable name="data">
            <xsl:copy-of select="*"/>
          </xsl:variable>
          <xsl:value-of select="json:encode($data)"/>
        </xsl:template>
      ');
      $xslt->setParameter('', 'isDirty', $isDirty);
      $result = $xslt->transformToXml($this->getDocument($xml));
      $this->assertEquals(
        $json,
        $result
      );
    }

    public static function provideStringExamples() {
      return array(
        'simple string' => array('"abc"', 'abc'),
        'with quotes' => array('"abc \\" \' def"', 'abc " \' def'),
        'line breaks' => array('"abc \\n \\n \\n def"', "abc \r\n \r \n def"),
        'comment' => array('"<!-" + "- comment -" + "->"', '<!-- comment -->')
      );
    }

    public static function provideXmlToJsonExamples() {
      $xmlns = 'xmlns:json="urn:carica-json-dom.2013"';
      return array(
        array(
          '""',
          '<data></data>'
        ),
        array(
          '{"foo":"bar"}',
          '<data><foo>bar</foo></data>'
        ),
        array(
          '{"foo":42}',
          '<data '.$xmlns.'><foo json:type="number">42</foo></data>'
        ),
        array(
          '{"foo":true}',
          '<data '.$xmlns.'><foo json:type="boolean">true</foo></data>'
        ),
        array(
          '{"foo":false}',
          '<data '.$xmlns.'><foo json:type="boolean">false</foo></data>'
        ),
        array(
          '{"list":[1,1.1,"foo",null,{}]}',
          '<data '.$xmlns.'>'.
          '<list json:type="array">'.
          '<_ json:type="number">1</_>'.
          '<_ json:type="number">1.1</_>'.
          '<_ json:type="string">foo</_>'.
          '<_ json:type="null"/>'.
          '<_ json:type="object"/>'.
          '</list>'.
          '</data>'
        )
      );
    }

    private function getDocument($xml = '<root/>') {
      $dom = new \DOMDocument();
      $dom->loadXml($xml);
      return $dom;
    }

    private function getProcessor($template, $method = 'text') {
      $stylesheet =
        '<xsl:stylesheet
           version="1.0"
           xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
           xmlns:json="urn:carica-json-dom.2013">

           <xsl:import href="'.htmlspecialchars($this->getWriterFilename()).'"/>

           <xsl:output method="'.htmlspecialchars($method).'"/>

           '.trim($template).'
         </xsl:stylesheet>';
      $dom = new \DOMDocument();
      $dom->loadXML($stylesheet);
      $xslt = new \XSLTProcessor();
      $xslt->importStylesheet($dom);
      return $xslt;
    }

    private function getWriterFilename() {
      return 'data://text/xml;base64,'.
        base64_encode(file_get_contents(__DIR__.'/../../../src/Carica/JsonDOM/Writer.xsl'));
    }
  }
}