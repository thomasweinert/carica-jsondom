<?php

namespace Carica\JsonDOM {

  include(__DIR__ . '/../../../src/Carica/JsonDOM/Writer.php');

  class WriterTest extends \PHPUnit_Framework_TestCase {

    /**
     * @dataProvider provideXmlToJsonExamples
     * @param string $json
     * @param string $xml
     */
    public function testXmlToJson($json, $xml, $isDirty = FALSE) {
      $dom = new \DOMDocument();
      $dom->loadXml($xml);
      $writer = new Writer($dom, $isDirty);
      $this->assertEquals(
        $json,
        json_encode($writer)
      );
    }

    public static function provideXmlToJsonExamples() {
      $xmlns = 'xmlns:json="urn:carica-json-dom.2013"';
      return array(
        array(
          '""',
          '<json/>'
        ),
        array(
          '""',
          '<json type="object"/>'
        ),
        array(
          '{}',
          '<json type="object"/>',
          TRUE // allow type without namespace
        ),
        array(
          '{}',
          '<json '.$xmlns.' json:type="object"/>'
        ),
        array(
          '{"key":""}',
          '<json '.$xmlns.'><i json:name="key"/></json>'
        ),
        array(
          '{"key":""}',
          '<json '.$xmlns.'><key/></json>'
        ),
        array(
          '{"key":null}',
          '<json '.$xmlns.'><key json:type="null"/></json>'
        ),
        array(
          '[]', '<json '.$xmlns.' json:type="array"/>'
        ),
        array(
          '["foo","bar"]',
          '<json '.$xmlns.' json:type="array"><_>foo</_><_>bar</_></json>'
        ),
        array(
          '[42,42.21]',
          '<json '.$xmlns.' json:type="array"><_ json:type="number">42</_><_ json:type="number">42.21</_></json>'
        ),
        array(
          '[42,42.21]',
          '<json '.$xmlns.' json:type="array"><_ json:type="int">42</_><_ json:type="float">42.21</_></json>'
        ),
        array(
          '[42,42.21]',
          '<json '.$xmlns.' json:type="array"><_ json:type="integer">42</_><_ json:type="double">42.21</_></json>'
        ),
        array(
          '[{},{"foobar":""}]',
          '<json '.$xmlns.' json:type="array"><foo json:type="object"/><bar><foobar/></bar></json>'
        ),
        array(
          '{"foo":[]}',
          '<json '.$xmlns.'><foo json:type="array"/></json>'
        )
      );
    }
  }
}
