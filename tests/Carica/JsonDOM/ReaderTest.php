<?php

namespace Carica\JsonDOM {

  include(__DIR__ . '/../../../src/Carica/JsonDOM/Reader.php');

  /**
   * @covers Carica\JsonDOM\Reader
   */
  class ReaderTest extends \PHPUnit_Framework_TestCase {

    public function testReaderWithEmptyObject() {
      $reader = new Reader('{}');
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013" json:type="object"/>',
        $dom->saveXML($dom->documentElement)
      );
    }

    public function testReaderWithEmptyArray() {
      $reader = new Reader('[]');
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013" json:type="array"/>',
        $dom->saveXML($dom->documentElement)
      );
    }

    public function testReaderWithArrayOfTwoObjectsWithStringProperty() {
      $reader = new Reader('[{"name":"foo"},{"name":"bar"}]');
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013" json:type="array">'.
        '<_>'.
        '<name>foo</name>'.
        '</_>'.
        '<_>'.
        '<name>bar</name>'.
        '</_>'.
        '</json:json>',
        $dom->saveXML($dom->documentElement)
      );
    }

    public function testReaderWithArrayOfTwoObjectsWithStringPropertyVerbose() {
      $reader = new Reader('[{"name":"foo"},{"name":"bar"}]', TRUE);
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013" json:type="array">'.
        '<_ json:type="object">'.
        '<name json:name="name" json:type="string">foo</name>'.
        '</_>'.
        '<_ json:type="object">'.
        '<name json:name="name" json:type="string">bar</name>'.
        '</_>'.
        '</json:json>',
        $dom->saveXML($dom->documentElement)
      );
    }

    public function testReaderWithObjectWithBooleanProperties() {
      $reader = new Reader('{"boolean-true":true,"boolean-false":false}');
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013">'.
        '<boolean-true json:type="boolean">true</boolean-true>'.
        '<boolean-false json:type="boolean">false</boolean-false>'.
        '</json:json>',
        $dom->saveXML($dom->documentElement)
      );
    }

    public function testReaderWithObjectWithNumericProperties() {
      $reader = new Reader('{"integer":42,"float":42.21}');
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013">'.
        '<integer json:type="number">42</integer>'.
        '<float json:type="number">42.21</float>'.
        '</json:json>',
        $dom->saveXML($dom->documentElement)
      );
    }

    public function testReaderWithObjectWithNullProperty() {
      $reader = new Reader('{"empty":null}');
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013">'.
        '<empty json:type="null"/>'.
        '</json:json>',
        $dom->saveXML($dom->documentElement)
      );
    }

    public function testReaderWithInvalidNCNameAsKey() {
      $reader = new Reader('{":::::":null}');
      $dom = $reader->asDom();
      $this->assertEquals(
        '<json:json xmlns:json="urn:carica-json-dom.2013">'.
        '<_ json:name=":::::" json:type="null"/>'.
        '</json:json>',
        $dom->saveXML($dom->documentElement)
      );
    }
  }
}