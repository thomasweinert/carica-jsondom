<?php

namespace Carica\JsonDOM {

  /**
   * Class Reader
   *
   * This class allows to transfer an JSON data structure from a string into a DOMDocument.
   *
   * Basic Usage:
   *
   * $reader = new \Carica\JsonDOM\Reader($jsonString);
   * $dom = $reader->asDom();
   *
   * @package Carica\JsonDOM
   */
  class Reader {

    const XMLNS = 'urn:carica-json-dom.2013';
    const DEFAULT_QNAME = '_';

    /**
     * Maximum recursions
     *
     * @var int
     */
    private $_recursions = 100;

    /**
     * Buffer for the decoded json
     *
     * @var array|object
     */
    private $_json = '';

    /**
     * Add json:type and json:name attributes to all elements, even if not necessary.
     *
     * @var bool
     */
    private $_verbose = FALSE;

    /**
     * Create the reader for a json string. The string will be decoded into a php variable structure.
     *
     * If verbose is TRUE, the DOMNodes will all have
     * json:type and json:name attributes. Even if the information could be read from the structure.
     *
     * @param string $json
     * @param bool $verbose
     * @param int $depth
     */
    public function __construct($json, $verbose = FALSE, $depth = 100) {
      $this->_recursions = $depth;
      $this->_json = json_decode($json, FALSE, $depth);
      $this->_verbose = (bool)$verbose;
    }

    /**
     * Get a DOM created from the JSON
     *
     * @return \DOMDocument
     */
    public function asDom() {
      $dom = new \DOMDocument('1.0', 'UTF-8');
      $this->importTo($dom);
      return $dom;
    }

    /**
     * Import the JSON data into an existing DOM, this will remove the current document element
     * of the DOM and append a new one.
     *
     * @param \DOMDocument $dom
     */
    public function importTo(\DOMDocument $dom) {
      if ($dom->documentElement) {
        $dom->removeChild($dom->documentElement);
      }
      $dom->appendChild(
        $root = $dom->createElementNS(self::XMLNS, 'json:json')
      );
      $this->transferTo($root, $this->_json, $this->_recursions);
    }

    /**
     * Transfer a value into a target xml element node. This sets attributes on the
     * target node and creates child elements for object and array values.
     *
     * If the current element is an object or array the method is called recursive.
     * The $recursions parameter is used to limit the recursion depth of this function.
     *
     * @param \DOMElement $target
     * @param $value
     * @param int $recursions
     */
    private function transferTo(\DOMElement $target, $value, $recursions = 100) {
      if ($recursions < 1) {
        return;
      }
      if (is_array($value)) {
        $this->transferArrayTo($target, $value, $this->_recursions - 1);
      } elseif (is_object($value)) {
        $this->transferObjectTo($target, $value, $this->_recursions - 1);
      } elseif (is_null($value)) {
        $target->setAttributeNS(self::XMLNS, 'json:type', 'null');
      } elseif (is_bool($value)) {
        $target->setAttributeNS(self::XMLNS, 'json:type', 'boolean');
        $target->appendChild(
          $target->ownerDocument->createTextNode($value ? 'true' : 'false')
        );
      } elseif (is_int($value) || is_float($value)) {
        $target->setAttributeNS(self::XMLNS, 'json:type', 'number');
        $target->appendChild($target->ownerDocument->createTextNode((string)$value));
      } else {
        if ($this->_verbose) {
          $target->setAttributeNS(self::XMLNS, 'json:type', 'string');
        }
        $target->appendChild($target->ownerDocument->createTextNode((string)$value));
      }
    }

    /**
     * Transfer an array value into a target element node. Sets the json:type attribute to 'array' and
     * creates child element nodes for each array element using the default QName.
     *
     * @param \DOMElement $target
     * @param array $value
     * @param int $recursions
     */
    private function transferArrayTo(\DOMElement $target, array $value, $recursions) {
      $target->setAttributeNS(self::XMLNS, 'json:type', 'array');
      foreach ($value as $item) {
        $target->appendChild(
          $child = $target->ownerDocument->createElement(self::DEFAULT_QNAME)
        );
        $this->transferTo($child, $item, $recursions);
      }
    }

    /**
     * Transfer an object value into a target element node. If the object has no properties,
     * the json:type attribute is always set to 'object'. If verbose is not set the json:type attribute will
     * be omitted if the object value has properties.
     *
     * The method creates child nodes for each property. The property name will be normalized to a valid NCName.
     * If the normalized NCName is different from the property name or verbose is TRUE, a json:name attribute
     * with the property name will be added.
     *
     * The {@see Writer::transferTo()} is called for each property value using the created child node.
     *
     * @param \DOMElement $target
     * @param object $value
     * @param int $recursions
     */
    private function transferObjectTo(\DOMElement $target, $value, $recursions) {
      $properties = get_object_vars($value);
      if ($this->_verbose || empty($properties)) {
        $target->setAttributeNS(self::XMLNS, 'json:type', 'object');
      }
      foreach ($properties as $property => $item) {
        $qname = $this->normalizeKey($property);
        $target->appendChild(
          $child = $target->ownerDocument->createElement($qname)
        );
        if ($this->_verbose || $qname != $property) {
          $child->setAttributeNS(self::XMLNS, 'json:name', $property);
        }
        $this->transferTo($child, $item, $recursions);
      }
    }

    /**
     * Removes all characters from a json key string that are not allowed in a xml NCName. An NCName is the
     * tag name of an xml element without a prefix.
     *
     * If the result of that removal is an empty string, the default QName is returned.
     *
     * @param string $key
     * @return string
     */
    private function normalizeKey($key) {
      $nameStartChar =
        'A-Z_a-z'.
        '\\x{C0}-\\x{D6}\\x{D8}-\\x{F6}\\x{F8}-\\x{2FF}\\x{370}-\\x{37D}'.
        '\\x{37F}-\\x{1FFF}\\x{200C}-\\x{200D}\\x{2070}-\\x{218F}'.
        '\\x{2C00}-\\x{2FEF}\\x{3001}-\\x{D7FF}\\x{F900}-\\x{FDCF}'.
        '\\x{FDF0}-\\x{FFFD}\\x{10000}-\\x{EFFFF}';
      $nameAdditionalChar =
        $nameStartChar.
        '\\.\\d\\x{B7}\\x{300}-\\x{36F}\\x{203F}-\\x{2040}';
      $result = preg_replace(
        array(
          '([^'.$nameAdditionalChar.'-]+)u',
          '(^[^'.$nameStartChar.']+)u',
        ),
        '',
        $key
      );
      return (empty($result)) ? self::DEFAULT_QNAME : $result;
    }
  }
}