<?php

namespace Carica\JsonDOM {

  /**
   * Encapsulates a DOMDocument or DOMElement to make it JsonSerializeable.
   */
  class Writer implements \JsonSerializable {

    const XMLNS = 'urn:carica-json-dom.2013';

    /**
     * @var \DOMNode
     */
    private $_source = NULL;
    /**
     * @var bool
     */
    private $_isDirty = FALSE;
    /**
     * @var \DOMXpath
     */
    private $_xpath = NULL;

    /**
     * Encapsulates a DOMDocument or DOMElement to make it JsonSerializeable. The "name" is used as key if provided.
     * The attribute "type" defines the output type of the value. The attributes are expected in the JsonDOM
     * namespace by default. If $isDirty is TRUE the attributes without a namespace are used as a fallback.
     *
     * All other attributes are ignored. The name attribute will be checked and if it does not exists
     * the element name will be used as key value in the json structure. The top level and elements of
     * an array do not have keys.
     *
     * If the type-Attribute is not provided the value is considered a object if it has child element nodes,
     * a string otherwise.
     *
     * @param \DOMNode $source
     * @param bool $dirty
     */
    public function __construct(\DOMNode $source, $dirty = FALSE) {
      if (!($source instanceof \DOMDocument || $source instanceof \DOMElement)) {
        throw new \InvalidArgumentException('A dom document or element are needed.');
      }
      $this->_source = $source;
      $this->_isDirty = (bool)$dirty;
    }

    /**
     * Implement the JSONSerialize interface, provide a data structure that can be used with json_encode().
     *
     * @return array|bool|float|int|mixed|null|\stdClass|string
     */
    public function jsonSerialize() {
      if ($this->_source instanceOf \DOMDocument) {
        $node = $this->_source->documentElement;
      } elseif ($this->_source instanceOf \DOMElement) {
        $node = $this->_source;
      }
      if (isset($node)) {
        return $this->getValueFromNode($node);
      }
      return NULL;
    }

    /**
     * The Xpath object is used during the serialization process to check for child elements and fetch them.
     *
     * This method created the DOMXpath instance if needed and registers the JsonDOM namespace for it.
     *
     * @param $node
     * @return \DOMXpath
     */
    private function xpath($node) {
      if (!isset($this->_xpath) || $this->_xpath->document != $node->ownerDocument) {
        $this->_xpath = new \DOMXpath($node->ownerDocument);
        $this->_xpath->registerNamespace('json', self::XMLNS);
      }
      return $this->_xpath;
    }

    /**
     * Get the value for the specified target node.
     *
     * @param \DOMElement $node
     * @return array|bool|float|int|\stdClass|string
     */
    private function getValueFromNode(\DOMElement $node) {
      $type = $this->getTypeFromNode($node);
      switch ($type) {
      case 'null' :
        return NULL;
      case 'bool' :
      case 'boolean' :
        return $node->nodeValue == 'yes';
      case 'int' :
      case 'integer' :
      case 'long' :
        return (int)$node->nodeValue;
      case 'float' :
      case 'double' :
      case 'real' :
        return (float)$node->nodeValue;
      case 'number' :
        if (FALSE === strpos($node->nodeValue, '.')) {
          return (int)$node->nodeValue;
        } else {
          return (float)$node->nodeValue;
        }
      case 'array' :
        $result = array();
        foreach ($node->childNodes as $child) {
          $result[] = $this->getValueFromNode($child);
        }
        return $result;
      case 'object' :
        $result = new \stdClass;
        foreach ($this->xpath($node)->evaluate('./*', $node, FALSE) as $child) {
          $name = $this->getKeyFromNode($child);
          $result->{$name} = $this->getValueFromNode($child);
        }
        return $result;
      case 'string' :
      default :
        return (string)$node->nodeValue;
      }
    }

    /**
     * Get the type of its value from a node. This will read the json:type attribute first,
     * the type attribute second if $isDirty is TRUE. If the attribute does not exists it will
     * check for child element nodes. Nodes with child elements are by default objects, all others are
     * considered strings.
     *
     * @param \DOMElement $node
     * @return string
     */
    private function getTypeFromNode(\DOMElement $node) {
      if ($node->hasAttributeNS(self::XMLNS, 'type')) {
        return $node->getAttributeNodeNS(self::XMLNS, 'type')->value;
      } elseif ($this->_isDirty) {
        return $node->getAttributeNode('type')->value;
      } elseif ($this->xpath($node)->evaluate('count(./*)', $node, FALSE) > 0) {
        return 'object';
      } else {
        return 'string';
      }
    }

    /**
     * Gets the json key string from the node. This will read the json:name attribute first,
     * the name attribute second if $isDirty is TRUE, if the attribute does not exists the local tag name
     * (without namespace) is used.
     *
     * If the name ist empty '_' is returned.
     *
     * @param \DOMElement $node
     * @return \DOMAttr|string
     */
    private function getKeyFromNode(\DOMElement $node) {
      if ($node->hasAttributeNS(self::XMLNS, 'name')) {
        $name = $node->getAttributeNodeNS(self::XMLNS, 'name')->value;
      } elseif ($this->_isDirty) {
        $name = $node->getAttributeNode('name')->value;
      } else {
        $name = $node->localName;
      }
      return (trim($name) == '') ? '_' : $name;
    }
  }
}