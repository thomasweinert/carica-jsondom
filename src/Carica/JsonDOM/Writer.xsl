<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exsl="http://exslt.org/common"
  xmlns:func="http://exslt.org/functions"
  xmlns:str="http://exslt.org/strings"
  xmlns:json="urn:carica-json-dom.2013"
  extension-element-prefixes="exsl func str"
  exclude-result-prefixes="json">

  <func:function name="json:encode">
    <xsl:param name="values"/>
    <xsl:param name="isDirty" select="false()"/>
    <func:result select="json:encode-node(exsl:node-set($values), $isDirty)"/>
  </func:function>

  <func:function name="json:encode-node">
    <xsl:param name="node"/>
    <xsl:param name="isDirty" select="false()"/>
    <xsl:variable name="type" select="json:getTypeFromNode($node)"/>
    <xsl:variable name="result">
      <xsl:choose>
        <xsl:when test="$type = 'object'">
          <xsl:value-of select="json:encode-object($node, $isDirty)"/>
        </xsl:when>
        <xsl:when test="$type = 'array'">
          <xsl:value-of select="json:encode-array($node, $isDirty)"/>
        </xsl:when>
        <xsl:when test="$type = 'number'">
          <xsl:value-of select="json:encode-number($node)"/>
        </xsl:when>
        <xsl:when test="$type = 'int' or $type = 'integer'">
          <xsl:value-of select="json:encode-number($node)"/>
        </xsl:when>
        <xsl:when test="$type = 'float' or $type = 'real' or $type = 'double'">
          <xsl:value-of select="json:encode-number($node)"/>
        </xsl:when>
        <xsl:when test="$type = 'bool' or $type = 'boolean'">
          <xsl:value-of select="json:encode-boolean($node)"/>
        </xsl:when>
        <xsl:when test="$type = 'null'">
          <xsl:text>null</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="json:encode-string($node)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <func:result select="$result"/>
  </func:function>

  <func:function name="json:encode-object">
    <xsl:param name="node"/>
    <xsl:param name="isDirty" select="false()"/>
    <xsl:variable name="result">
      <xsl:text>{</xsl:text>
      <xsl:for-each select="$node/*">
        <xsl:if test="position() &gt; 1">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of select="json:encode-string(json:getNameFromNode(., $isDirty))"/>
        <xsl:text>:</xsl:text>
        <xsl:value-of select="json:encode-node(., $isDirty)"/>
      </xsl:for-each>
      <xsl:text>}</xsl:text>
    </xsl:variable>
    <func:result select="$result"/>
  </func:function>

  <func:function name="json:encode-array">
    <xsl:param name="node"/>
    <xsl:param name="isDirty" select="false()"/>
    <xsl:variable name="result">
      <xsl:text>[</xsl:text>
      <xsl:for-each select="$node/*">
        <xsl:if test="position() &gt; 1">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of select="json:encode-node(., $isDirty)"/>
      </xsl:for-each>
      <xsl:text>]</xsl:text>
    </xsl:variable>
    <func:result select="$result"/>
  </func:function>

  <func:function name="json:encode-string">
    <xsl:param name="value" select="''"/>
    <xsl:variable name="quote">&quot;</xsl:variable>
    <xsl:variable name="search">
      <_>\</_>
      <_><xsl:value-of select="$quote"/></_>
      <_><xsl:text>&#13;</xsl:text></_>
      <_><xsl:text>&#10;</xsl:text></_>
      <_>--</_>
    </xsl:variable>
    <xsl:variable name="replace">
      <_>\\</_>
      <_>\"</_>
      <_>\r</_>
      <_>\n</_>
      <_>-<xsl:value-of select="$quote"/> + <xsl:value-of select="$quote"/>-</_>
    </xsl:variable>
    <func:result
      select="concat($quote, str:replace($value, exsl:node-set($search)/*, exsl:node-set($replace)/*), $quote)"/>
  </func:function>

  <func:function name="json:encode-number">
    <xsl:param name="value" select="0"/>
    <func:result select="number($value)"/>
  </func:function>

  <func:function name="json:encode-boolean">
    <xsl:param name="value" select="false()"/>
    <xsl:variable
      name="normalized"
      select="translate(string($value), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
    <xsl:variable name="result">
      <xsl:choose>
        <xsl:when test="$normalized = 'true'">true</xsl:when>
        <xsl:when test="$normalized = 'false'">false</xsl:when>
        <xsl:when test="$normalized = 'yes'">true</xsl:when>
        <xsl:when test="$normalized = 'no'">false</xsl:when>
        <xsl:when test="$normalized = 't'">true</xsl:when>
        <xsl:when test="$normalized = 'f'">false</xsl:when>
        <xsl:when test="$normalized = 'y'">true</xsl:when>
        <xsl:when test="$normalized = 'n'">false</xsl:when>
        <xsl:when test="number($value) &gt; 0">true</xsl:when>
        <xsl:when test="$value = true()">true</xsl:when>
        <xsl:otherwise>false</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <func:result select="$result"/>
  </func:function>

  <func:function name="json:getTypeFromNode">
    <xsl:param name="node"/>
    <xsl:param name="isDirty" select="false()"/>
    <xsl:variable name="result">
      <xsl:choose>
        <xsl:when test="$node/@json:type">
          <xsl:value-of select="$node/@json:type"/>
        </xsl:when>
        <xsl:when test="$isDirty and $node/@type">
          <xsl:value-of select="$node/@type"/>
        </xsl:when>
        <xsl:when test="count($node/*) &gt; 0">
          <xsl:text>object</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>string</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <func:result select="$result"/>
  </func:function>

  <func:function name="json:getNameFromNode">
    <xsl:param name="node"/>
    <xsl:param name="isDirty" select="false()"/>
    <xsl:variable name="result">
      <xsl:choose>
        <xsl:when test="$node/@json:name">
          <xsl:value-of select="$node/@json:name"/>
        </xsl:when>
        <xsl:when test="$isDirty and $node/@name">
          <xsl:value-of select="$node/@name"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="local-name($node)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <func:result select="$result"/>
  </func:function>

</xsl:stylesheet>